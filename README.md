Simple C++ implementation of a parser to solve basic arithmetic expressions.

> Arithmetic expressions consist of decimal integers (to simplify, only numbers from 0 to 9 are allowed as literals, i.e. neither multi-digit nor fractional numbers are allowed as literals). combined using the binary operators +, *, /, - and parentheses and should respect the usual precedence relations.


My approach is inspired by this tutorial http://effbot.org/zone/simple-top-down-parsing.html which states:

> Vaughan Pratt published an elegant improvement to recursive-descent in his paper Top-down Operator Precedence. Pratt’s algorithm associates semantics with tokens instead of grammar rules, and uses a simple “binding power” mechanism to handle precedence levels.

Not having known any Parsing Algorithms (since CS I back in my studies) I very
much vibed with that, as it meant i did not have to build an algorithm to
generate a parse-table, nor do backtracking. Also the algorithm makes do with
very few calls.

On the downside, I think error handling is done implicitly, which is elegant for
this small scope, but probably problematic if one wanted to extend the grammar.

As I am relatively new to C++, I did a rough reference implementation in python first.
Tokens.py defining the token classes.

## C++ Solution

compile

> g++ main.cpp Parser.cpp

run:

> ./a.out <program>

e.g. with program being <1+3+2>

*Tests are run by default and can be disabled in main.cpp*

**Asterisks***

The parser does not reject all invalid input patterns correctly ( "1+1)+3"=>2) .
I think this is (I am 80% certain), also the case for the algorithm I based my
solution on and I only realized that when I was way into it already. I didnt
change my solution then because I thought the point was not to be perfect, but
produce something representative, (but on branch minimalFix I introduce
some code to fix that bug, while being minimally invasive)


