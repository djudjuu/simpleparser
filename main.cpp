#include <iostream>
#include <vector>
#include <string>
#include "Parser.h"
#include "Test.cpp"

using namespace std;

int main(int argc, char** argv)
{
	testParser();
	string program = argv[1];

	Parser * p = new Parser(program);
	int result = p->run();

	cout << "========"<<endl;
	cout << ">" << program << "=" << result << endl;
	return 0;
}

