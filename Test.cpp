#include <iostream>
#include <vector>
#include <string>
#include "Parser.h"

using namespace std;

bool testParser()
{
	int failed = 0;
	Parser * p = new Parser("1");

	cout << "=========== positive TESTS =================== " << std::endl;
	vector<pair<string,int>> testCases;
	testCases.push_back(make_pair("1", 1));
	testCases.push_back(make_pair("1+3", 4));
	testCases.push_back(make_pair("1*2", 2));
	testCases.push_back(make_pair("2*2+4", 8));
	testCases.push_back(make_pair("2*(2+4)", 12));
	testCases.push_back(make_pair("2*(2+4)/2", 6));
	testCases.push_back(make_pair("(4+5*(7-3))-2", 22));
	testCases.push_back(make_pair("4+5+7/2", 12));

	for (auto t: testCases)
	{
		cout << t.first;
		int r = p->run(t.first);
		if (t.second != r) {
			cout << "test  " <<t.first << "failed!: Expected: " << t.second << " but got " << r <<  endl;
			failed++;
		} else {
			cout << "="<< r << " ... is correct!" << endl;
		}
	};

	cout << "=========== negative TESTS =================== " << std::endl;
	vector<pair<string,string>> negTestCases;
	negTestCases.push_back(make_pair("10+1","literal too long"));
	negTestCases.push_back(make_pair("1++1","Illegal Operand"));
	negTestCases.push_back(make_pair("1%1","Unknown type of input symbol"));
	negTestCases.push_back(make_pair("-1", "Illegal Operand"));
	negTestCases.push_back(make_pair("(2*3","Missing ')'"));
	negTestCases.push_back(make_pair("(","Illegal Operand"));

	for (auto t: negTestCases)
	{
		cout << t.first;
		int r;
		try {
			r = p->run(t.first);
		} catch (std::invalid_argument err) {
			string errmsg = string(err.what());
			bool errNotFound = errmsg.find(t.second)== std::string::npos;
			if (errNotFound) {
				failed++;
			} else {
				cout << "  ...errorred correctly (" << t.second << ")" << std::endl;
				continue;
			}
		}
		catch (const std::exception exc) {
			failed++;
			cout << "  FAILED with unexpected exception: " <<  endl;
			continue;
		}
		cout << "..." << "FAILED!: Expected error: " << t.second << " but got "<< r <<  endl;
	}

	cout << "================ TESTS RESULT ===============" << endl;
	if (failed ==0)
		cout << "All tests passed!" << endl;
	else
		cout <<"========> " << failed << " tests failed" << endl;
	return (failed == 0);
}
