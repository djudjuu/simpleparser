#include "Parser.h"

using namespace std;

Parser::Parser(std::string _program)
{
	m_program = _program;
}

int Parser::run()
{
	createAstNodes();
	m_token = m_tokenList.begin();
	m_ast = parseExpression();
	m_result = evaluate(m_ast);
	return m_result;
}

int Parser::run(string _program)
{
	m_program = _program;
	m_tokenList.clear();
	return run();
}

void Parser::createAstNodes()
{
	for(auto c = m_program.cbegin(); c != m_program.cend(); ++c)
	{
		if (isdigit(*c))
			m_tokenList.push_back(make_shared<Literal>(*c-'0'));
		else
		{
			switch(*c)
			{
				case '+':
					m_tokenList.push_back(make_shared<Operator>(ADD, 10));
					break;
				case '/':
					m_tokenList.push_back(make_shared<Operator>(DIV, 20));
					break;
				case '*':
					m_tokenList.push_back(make_shared<Operator>(MUL, 20));
					break;
				case '-':
					m_tokenList.push_back(make_shared<Operator>(SUB, 10));
					break;
				case '(':
					m_tokenList.push_back(make_shared<Paren>(LPAREN, 30));//Operator(LPAREN, 30));
					 break;
				case ')':
					m_tokenList.push_back(make_shared<Paren>(RPAREN, 0));//Operator(RPAREN,0));
					break;
				default:
					throw std::invalid_argument("ERROR: Unknown type of input symbol");
			}
		}
	}
	m_tokenList.push_back(make_shared<Operator>(END, -2));
}

AstPointer Parser::parseExpression(int rbp){
	AstPointer t = *(m_token++); // save previous node and then advance iterator
	AstPointer left = t->type == LPAREN ? parseParenthesis(t->type) : t->nud();
	while (rbp < (*m_token)->lbp())
	{
		t = *(m_token++);
		left = t->led(left, parseExpression(t->lbp()));
	}
	return left;
}

AstPointer Parser::parseParenthesis(nodeType _ptype)
{
	AstPointer left = parseExpression();
	if ((*m_token++)->type != RPAREN)
		throw std::invalid_argument("ERROR: Invalid input: Missing ')'");
	return left;
}

int Parser::evaluate(AstPointer _node)
{
	Operator* n = &*dynamic_pointer_cast<Operator>(_node); //weirdness, probably this can be avoided by using templates somehow
	switch(_node->type)
	{
		case LIT : return _node->value();
		case ADD: return evaluate(n->m_left) + evaluate(n->m_right);
		case SUB:  return evaluate(n->m_left) - evaluate(n->m_right);
		case MUL:  return evaluate(n->m_left) * evaluate(n->m_right);
		case DIV:  return evaluate(n->m_left) / evaluate(n->m_right);
		default: throw std::invalid_argument("Illegal type of ASTNode in traversed tree");
	}
}
