#ifndef PARSER_H
#define PARSER_H

#include <iostream>
#include <vector>
#include <string>
#include <cctype>
#include "AST.h"

class Parser {
public:
	Parser(std::string _program);
	~Parser();
	int run();
	int run(std::string _program);
private:
	// turns the symbols of the given program into a list of AstNodes
	void createAstNodes();
	// recursively processes symbols in the list of AstNodes thereby linking them into the AST
	AstPointer parseExpression(int rbp=0);
	// same as parseExpression but looks ahead for a closing parenthesis
	// Throws if no matching parenthesis is found
	AstPointer parseParenthesis(nodeType _ptype);
	// evaluates a subtree of Ast and returns the result of the program
	int evaluate(AstPointer);

	// member variables
	// input
	std::string m_program;
	// list of symbols/tokens turned into their respective astNodes
	std::vector<AstPointer> m_tokenList;
	// current symbol to be processed
	std::vector<AstPointer>::iterator m_token;
	int m_result;
	// pointer to the root of the AST
	AstPointer m_ast = nullptr;
};

#endif // PARSER_H
