#!/bin/python
import re

# ========== parse function =========
def expression(rbp=0):
    global token
    t = token
    # print '==== epx call with rbp ', rbp, ' =========='
    # print 'current token.nud', t.nud()
    token = next()
    # print "Called next"
    # print 'token', t#, ', next: ', token
    left = t.nud()
    # print 'left, ', left
    while rbp < token.lbp:
        # print 'comp:', rbp, token.lbp
        t = token
        token = next()
        # print "Called next"
        left = t.led(left)
    return left


class Token:
    def __init__(self, nodeType):
        self.nodeType = nodeType


# ========= classes =============
class lParen(Token):
    lbp = 150
    def nud(self):
        expr = expression()
        advance("rParen")
        return expr
    def __repr__(self):
        return "("

class rParen(Token):
    lbp = 0
    # def nud(self):
        # expr = expression()
        # advance(")")
        # return expr
    # def __repr__(self):
    #     return ")"


def advance(nodeType=None):
    global token
    if nodeType and token.nodeType != nodeType:
        print 'got:', token.nodeType
        raise SyntaxError("Expected %r" % nodeType)
    token = next()

class literal_token(Token):
    def __init__(self, value, nodeType):
        self.value = int(value)
        Token.__init__(self, nodeType)
    def nud(self):
        return self
    def __repr__(self):
        return "(literal %s)" % self.value

class operator_add_token(Token):
    lbp = 10
    def __init__(self, nodeType):
        Token.__init__(self, nodeType)

    def led(self, left):
        self.first = left
        self.second = expression(10)
        return self
    def __repr__(self):
        return "(add %s %s)" % (self.first, self.second)

class operator_mul_token(Token):
    lbp = 20
    def led(self, left):
        self.first = left
        self.second = expression(20)
        return self
    def __repr__(self):
        return "(mul %s %s)" % (self.first, self.second)

class end_token:
    lbp = 0


# ============ tokenizer ================


token_pat = re.compile("\s*(?:(\d+)|(.))")

def tokenize(program):
    print 'program:',  token_pat.findall(program)
    for number, operator in token_pat.findall(program):
        # print number, operator
        if number:
            yield literal_token(number, 'lit')
        elif operator == "+":
            yield operator_add_token('add')
        elif operator == "(":
            yield lParen('lParen')
        elif operator == ")":
            yield rParen('rParen')
        elif operator == "*":
            yield operator_mul_token('mul')
        else:
            raise SyntaxError("unknown operator")
    yield end_token()

def parse(program):
    global token, next
    next = tokenize(program).next
    token = next()
    return expression()

### evaluation

def evaluate(subtree):
    if subtree.nodeType == 'lit':
        return subtree.value
    elif subtree.nodeType == 'add':
        return evaluate(subtree.first) + evaluate(subtree.second)
    else:
        return evaluate(subtree.first) * evaluate(subtree.second)

program = [('2', ''), ('', '+'), ('3', ''), ('', '*'), ('1', '')]

# ast = parse("2*3+2")
inp = "2*(2+1)"
# inp = "2+3*1"
inp = "1+3+1"
inp = "2*(2+1)+3"
print "input", inp
ast = parse(inp)
print "------------ AST -----------"
print ast
print '========= eval =========='

print evaluate(ast)

