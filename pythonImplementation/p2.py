from Tokens import Op_add, Op_mul, Literal, End_token, LParen, RParen

class Parser:
    def __init__(self, inString=""):
        self.inString = inString
        self.tokenString = None
        self.ast = None
        self.result = None
        self.token = None
        self.index = 0
        self.tokenize()

    def reset(self):
        self.inString = self.tokenString = self.ast = None
        self.index = 0

    def run(self):
        self.parse()
        return self.evaluate()

    # create tokenClasses for all elements and set self.token to first symbol
    def tokenize(self):
        self.tokenString = []
        for x in self.inString:
            if x == '+':
                self.tokenString.append(Op_add())
            elif x == '*':
                self.tokenString.append(Op_mul())
            elif x in ['1', '2', '3', '4', '5','6']:
                self.tokenString.append(Literal(int(x)))
            elif x == '(':
                self.tokenString.append(LParen())
            elif x == ')':
                self.tokenString.append(RParen())
            else:
                raise SyntaxError('Illegal Token: ' + x)
        self.tokenString.append(End_token())
        self.token = self.nextToken()

    # emulates an iterator next() function
    def nextToken(self):
        self.index += 1
        return self.tokenString[self.index - 1]

    def parseExpression(self, rbp=0):
        print "parseSub called with: ", self.inString[self.index:], ' rbp: ', rbp
        t = self.token
        self.token = self.nextToken()
        left = self.parseParenthesis() if t.nodeType == 'lParen' else t.nud()
        while rbp < self.token.lbp:
            # print 'entered loop: ' , rbp, self.token.lbp
            t = self.token #+
            self.token = self.nextToken()
            left = t.led(left, self.parseExpression(t.lbp))
        return left

    def parseParenthesis(self):
        left = self.parseExpression(0)
        if self.token.nodeType != 'rParen':
            raise SyntaxError('missing <)> !! (instead: ', self.token.nodeType, ' )')
        self.token = self.nextToken()
        return left

    def parse(self):
        self.ast = self.parseExpression(0)

    def parseProgram(self, inString):
        self.reset()
        # print self.token, self.index, self.ast, self.inString
        self.inString = inString
        self.tokenize()
        # print self.token, self.index, self.ast, self.inString
        self.ast = self.parseExpression(0)
        self.evaluate()
        res = (self.ast, self.result)
        print res
        return res


    def evalSubtree(self, subtree):
        if subtree.nodeType == 'lit':
            return subtree.value
        elif subtree.nodeType == 'add':
            return self.evalSubtree(subtree.first) + self.evalSubtree(subtree.second)
        elif subtree.nodeType == 'mul':
            return self.evalSubtree(subtree.first) * self.evalSubtree(subtree.second)
        else:
            print "EEERRRRROOOORRR: unknown type of node", subtree.nodeType
            return 1
        # return evalSubtree(subtree.first) * evaluate(subtree.second)

    def evaluate(self):
        self.result = self.evalSubtree(self.ast)
        return self.result

       #do magic with self.ast


# truth: [tokenized Program, ast, result]
testCases = [
    ("1+1", ["(add (literal 1) (literal 1))", 2 ]), #simple 
    ("2*(2+1)", ["(mul (literal 2) (add (literal 2) (literal 1)))",6]), # parens
    ("2*(2+1)+3", ["(add (mul (literal 2) (add (literal 2) (literal 1))) (literal 3))",9]) # parens with something after
]
def testParser(testCase):
    print ' '
    print '========= testing ', testCase[0], ' ==========='
    expOutcome = testCase[1]
    p = Parser(testCase[0])
    # p.tokenize()
    # if p.tokenString != expOutcome[0]:
    #     print 'TOkenization ERROR:', p.tokenString, expOutcome[0]
    #     raise SyntaxError('tokenization')

    p.parse()
    if str(p.ast) != expOutcome[0]:
        print 'Parser ERROR:'
        print 'Expected:', expOutcome[0]
        print 'Obtained:', p.ast
        return

    r = p.evaluate()
    if r != expOutcome[1]:
        print 'Evaluation ERROR:'
        print 'Expected:', expOutcome[1]
        print 'Obtained:', p.result
        return
    print "       => All good"

# p = Parser("")
# for test in testCases:
    # testParser(test)


p = Parser("1)")
a = p.run()
print 'a', a


inp = "1)"#testCases[0][0]
print inp
# p.reset()
p.parseProgram(inp)
