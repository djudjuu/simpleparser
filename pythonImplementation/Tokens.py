class Token():
    def __init__(self, nodeType):
        self.nodeType = nodeType

    # @abstractmethod
    # def nud(self):
    #     pass

class Operand(Token):
    def __init__(self, nodeType):
        self.lbp = None
        Token.__init__(self, nodeType)

    # @abstractmethod
    # def led(self):
    #     pass

class Literal(Token):
    def __init__(self, value):
        self.value = value
        Token.__init__(self, "lit")

    def nud(self):
        return self
    def __repr__(self):
        return "(literal %s)" % self.value

class Op_add(Operand):
    def __init__(self):
        self.lbp = 10
        Token.__init__(self, 'add')

    def led(self, left, right):
        self.first = left
        self.second = right #expression(10)
        return self

    # would only needed for unary operations
    # def nud():

    def __repr__(self):
        return "(add %s %s)" % (self.first, self.second)

class Op_mul(Operand):

    def __init__(self):
        self.lbp = 20
        Token.__init__(self, 'mul')

    def led(self, left, right):
        self.first = left
        self.second = right # expression(20)
        return self
    def __repr__(self):
        return "(mul %s %s)" % (self.first, self.second)

class End_token(Token):

    def __init__(self):
        self.lbp = 0
        Token.__init__(self, 'end')

class LParen(Token):
    def __init__(self):
        self.lbp = 150
        Token.__init__(self, 'lParen')

    # def nud(self):
        # expr = expression()
        # advance("rParen")
        # return expr
    def __repr__(self):
        return "("

class RParen(Token):
    def __init__(self):
        self.lbp = 0
        Token.__init__(self, 'rParen')
 

