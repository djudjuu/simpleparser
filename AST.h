#ifndef ASTNODE_H
#define ASTNODE_H

#include <iostream>
#include <vector>
#include <string>
#include <cctype>
#include <memory>

//forward declaration
class AstNode;
using AstPointer = std::shared_ptr<AstNode>; // why does it not work to declare this above?

enum nodeType { MUL, ADD, SUB, DIV, LIT, LPAREN, RPAREN, END };

class AstNode : public std::enable_shared_from_this<AstNode>
{
public:
	AstNode(nodeType _type) {
		type = _type;
	}

	~AstNode() {
		//std::cout << "astNode " << getType() << " destructed" << std::endl;
	}

	virtual AstPointer nud()
	{
		std::string msg = "InputError: Illegal Operand." + getType() + "-token used as left side! \n"; //TODO add syntaxError
		throw std::invalid_argument(msg);
		return nullptr;
	}

	virtual AstPointer led(AstPointer _left, AstPointer _right)
	{
		std::string msg = "InputError: nodetype" + getType() + " used as operator (literal too long) \n";
		throw std::invalid_argument(msg);
		return nullptr;
	}

	virtual int lbp() {
			std::string msg = "InputError: nodetype: " + getType() + " used as operator (literal too long) \n";
			throw std::invalid_argument(msg);
			return -1;
	}

	virtual int value()
	{
		std::string msg = "Error: tried to get a value from nodeType" + getType() + "\n";
		throw std::invalid_argument(msg);
		return -1;
	}
	//debugging
	std::string getType()
	{
		switch (type)
		{
			case ADD: return "add";
			case MUL: return "mul";
			case DIV: return "div";
			case LPAREN: return "lparen";
			case RPAREN: return "rparen";
			case LIT: return "lit";
			case END: return "end";
			default: return "UNKNOWN";
		}
	}
	nodeType type;
	int m_value;
};


class Literal : public AstNode {
public:
	Literal(int _value)
	: AstNode(LIT) {
		m_value = _value;
	}
	AstPointer nud() {return shared_from_this();}
	int value() {return m_value;}
};

class Operator : public AstNode
{
public:
	Operator(nodeType _type, int _lbp)
	: AstNode(_type) {
		m_lbp = _lbp;
	}
	AstPointer led(AstPointer _left, AstPointer _right)
	{
		m_left = _left;
		m_right = _right;
		return shared_from_this();
	}
	int lbp() {return m_lbp;}
	AstPointer m_left;
	AstPointer m_right;
	int m_lbp;
};

class Paren : public AstNode {
public:
	Paren(nodeType type, int _lbp)
	: AstNode(type) {
		m_lbp = _lbp;
	}
	int lbp() {return m_lbp;}
	int m_lbp;
};

#endif // ASTNODE_H
